# OpenML dataset: BTC

https://www.openml.org/d/45972

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

### Description:
The `bitstampUSD_1-min_data_2012-01-01_to_2021-03-31.csv` dataset is a comprehensive collection of minute-by-minute Bitcoin trading data on the Bitstamp platform, spanning from January 1, 2012, to March 31, 2021. This dataset is crucial for understanding Bitcoin's trading dynamics over time, including price fluctuations and trading volumes. It is formatted in a chronological order, covering nearly a decade of Bitcoin transactions, making it an invaluable resource for financial analysts, econometric study, and anyone interested in the cryptocurrency market dynamics.

### Attribute Description:
- **Timestamp**: The date and time (Unix timestamp) of the observation.
- **Open**: The opening price of Bitcoin in USD at the start of the minute.
- **High**: The highest price of Bitcoin in USD during the minute.
- **Low**: The lowest price of Bitcoin in USD during the minute.
- **Close**: The closing price of Bitcoin in USD at the end of the minute.
- **Volume_(BTC)**: The volume of Bitcoin traded during the minute.
- **Volume_(Currency)**: The volume of trades in the corresponding currency (USD) during the minute.
- **Weighted_Price**: The volume-weighted average price of Bitcoin in USD during the minute.

### Use Case:
This dataset is crucial for various applications such as algorithmic trading, market analysis, academic research on financial markets, and cryptocurrency. It provides detailed insights for developing trading strategies based on minute-level market data, understanding price volatility, and studying market trends over a significant period. Furthermore, econometric models can be enhanced by utilizing this dataset to predict Bitcoin prices or understand the impact of global events on cryptocurrency markets. Additionally, educators and students can use this dataset for real-world case studies in finance and economics courses.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45972) of an [OpenML dataset](https://www.openml.org/d/45972). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45972/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45972/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45972/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

